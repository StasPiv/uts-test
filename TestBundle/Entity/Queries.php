<?php

namespace UTS\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Queries
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Queries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="query", type="string", length=255)
     */
    private $query;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="arriveDate", type="datetime")
     */
    private $arriveDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="leftDate", type="datetime")
     */
    private $leftDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="countDaysArrive", type="integer")
     */
    private $countDaysArrive;

    /**
     * @var integer
     *
     * @ORM\Column(name="countMen", type="integer")
     */
    private $countMen;

    /**
     * @var string
     *
     * @ORM\Column(name="userName", type="string", length=255)
     */
    private $userName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $time;

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    public function __construct()
    {
        $this->time = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set query
     *
     * @param string $query
     * @return Queries
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get query
     *
     * @return string 
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set arriveDate
     *
     * @param \DateTime $arriveDate
     * @return Queries
     */
    public function setArriveDate($arriveDate)
    {
        $this->arriveDate = $arriveDate;

        return $this;
    }

    /**
     * Get arriveDate
     *
     * @return \DateTime 
     */
    public function getArriveDate()
    {
        return $this->arriveDate;
    }

    /**
     * Set leftDate
     *
     * @param \DateTime $leftDate
     * @return Queries
     */
    public function setLeftDate($leftDate)
    {
        $this->leftDate = $leftDate;

        return $this;
    }

    /**
     * Get leftDate
     *
     * @return \DateTime 
     */
    public function getLeftDate()
    {
        return $this->leftDate;
    }

    /**
     * Set countDaysArrive
     *
     * @param integer $countDaysArrive
     * @return Queries
     */
    public function setCountDaysArrive($countDaysArrive)
    {
        $this->countDaysArrive = $countDaysArrive;

        return $this;
    }

    /**
     * Get countDaysArrive
     *
     * @return integer 
     */
    public function getCountDaysArrive()
    {
        return $this->countDaysArrive;
    }

    /**
     * Set countMen
     *
     * @param integer $countMen
     * @return Queries
     */
    public function setCountMen($countMen)
    {
        $this->countMen = $countMen;

        return $this;
    }

    /**
     * Get countMen
     *
     * @return integer 
     */
    public function getCountMen()
    {
        return $this->countMen;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return Queries
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUserName()
    {
        return $this->userName;
    }
}
