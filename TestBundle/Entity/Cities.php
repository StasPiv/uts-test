<?php

namespace UTS\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cities
 *
 * @ORM\Table(name="_cities", indexes={@ORM\Index(name="title_ru_index", columns={"title_ru"}), @ORM\Index(name="city_ru_ft", columns={"title_ru", "region_ru", "area_ru"})})
 * @ORM\Entity
 */
class Cities
{
    /**
     * @var integer
     *
     * @ORM\Column(name="country_id", type="integer", nullable=false)
     */
    private $countryId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="important", type="boolean", nullable=false)
     */
    private $important;

    /**
     * @var integer
     *
     * @ORM\Column(name="region_id", type="integer", nullable=true)
     */
    private $regionId;

    /**
     * @var string
     *
     * @ORM\Column(name="title_ru", type="string", length=150, nullable=true)
     */
    private $titleRu;

    /**
     * @var string
     *
     * @ORM\Column(name="area_ru", type="string", length=150, nullable=true)
     */
    private $areaRu;

    /**
     * @var string
     *
     * @ORM\Column(name="region_ru", type="string", length=150, nullable=true)
     */
    private $regionRu;

    /**
     * @var integer
     *
     * @ORM\Column(name="city_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cityId;

    /**
     * Set countryId
     *
     * @param integer $countryId
     * @return Cities
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * Get countryId
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set important
     *
     * @param boolean $important
     * @return Cities
     */
    public function setImportant($important)
    {
        $this->important = $important;

        return $this;
    }

    /**
     * Get important
     *
     * @return boolean 
     */
    public function getImportant()
    {
        return $this->important;
    }

    /**
     * Set regionId
     *
     * @param integer $regionId
     * @return Cities
     */
    public function setRegionId($regionId)
    {
        $this->regionId = $regionId;

        return $this;
    }

    /**
     * Get regionId
     *
     * @return integer 
     */
    public function getRegionId()
    {
        return $this->regionId;
    }

    /**
     * Set titleRu
     *
     * @param string $titleRu
     * @return Cities
     */
    public function setTitleRu($titleRu)
    {
        $this->titleRu = $titleRu;

        return $this;
    }

    /**
     * Get titleRu
     *
     * @return string 
     */
    public function getTitleRu()
    {
        return $this->titleRu;
    }

    /**
     * Set areaRu
     *
     * @param string $areaRu
     * @return Cities
     */
    public function setAreaRu($areaRu)
    {
        $this->areaRu = $areaRu;

        return $this;
    }

    /**
     * Get areaRu
     *
     * @return string 
     */
    public function getAreaRu()
    {
        return $this->areaRu;
    }

    /**
     * Set regionRu
     *
     * @param string $regionRu
     * @return Cities
     */
    public function setRegionRu($regionRu)
    {
        $this->regionRu = $regionRu;

        return $this;
    }

    /**
     * Get regionRu
     *
     * @return string 
     */
    public function getRegionRu()
    {
        return $this->regionRu;
    }

    /**
     * Get cityId
     *
     * @return integer 
     */
    public function getCityId()
    {
        return $this->cityId;
    }
}
