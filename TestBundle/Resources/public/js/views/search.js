/**
 * Created by Станислав on 01.04.14.
 */
var app = app || {};

app.SearchView = Backbone.View.extend({
    el: 'form[name="form"]',

    events: {
        'submit' : 'formSubmit',
        'change #form_arriveDate' : 'setMinLeftDate',
        'change #form_leftDate' : 'setMaxArriveDate',
        'change #form_countDaysArrive' : 'validateCountDaysArrive'
    },

    initialize: function() {
        this.$city = this.$('#form_city');
        this.$dateArrive = this.$('#form_arriveDate');
        this.$dateLeft = this.$('#form_leftDate');
        this.$countDaysArrive = this.$('#form_countDaysArrive');
        this.$countMen = this.$('#form_countMen');

        this.initPlugins();
    },

    initPlugins: function() {
        this.$city.autocomplete({
            source: SITE_ROOT + 'hotel/getcities'
        });

        this.$dateArrive.datepicker({
            'dateFormat' : 'dd-mm-yy'
        });

        this.$dateLeft.datepicker({
            'dateFormat' : 'dd-mm-yy'
        });

        this.initDates();
    },

    initDates: function() {
        var oneDayInMs = 86400000;

        this.$dateArrive.datepicker('option','minDate',
            new Date(new Date().getTime() + oneDayInMs));

        this.$dateLeft.datepicker('option','minDate',
            new Date(new Date().getTime() + 2*oneDayInMs));
    },

    formSubmit: function(e) {
        e.preventDefault();
        var form = this.el;
        $.ajax({
            type: $(form).attr('method'),
            dataType: 'json',
            data: $(form).serialize(),
            url: $(form).attr('action'),
            success: function(data) {
                if(data) {
                    alert('Запрос №' + data + ' выполнен');
                    form.reset();
                }
            }
        })
    },

    setMinLeftDate : function() {
        if(!this.$dateArrive.val()) {
            return false;
        }

        this.$dateLeft.datepicker('option','minDate',
            new Date(this.$dateArrive.datepicker('getDate').getTime() + 86400000));
    },

    setMaxArriveDate : function() {
        if(!this.$dateLeft.val()) {
            return false;
        }

        this.$dateArrive.datepicker('option','maxDate',
            new Date(this.$dateLeft.datepicker('getDate').getTime() - 86400000));
    },

    validateCountDaysArrive : function() {
        this.$countDaysArrive.val(this.$countDaysArrive.val().replace(/\D+/g,''));

        var maxDays = 1;
        var oneDayInMs = 86400000;

        if(this.$dateLeft.val() && this.$dateArrive.val()) {
            maxDays =
            (this.$dateLeft.datepicker('getDate').getTime() -
             this.$dateArrive.datepicker('getDate').getTime())
            /
            oneDayInMs;
        }

        if(this.$countDaysArrive.val() == 0) {
            this.$countDaysArrive.val(1);
        }

        if(this.$countDaysArrive.val() > maxDays) {
            this.$countDaysArrive.val(maxDays);
        }
    }
});