<?php

namespace UTS\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UTS\TestBundle\Entity\Queries;

class UserController extends Controller
{
    public function indexAction(Request $request)
    {
        $request->setLocale('ru');

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('make_query'))
            ->add('city', 'text')
            ->add('arriveDate', 'date', [
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => ['class' => 'date']
            ])
            ->add('leftDate', 'date', [
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => ['class' => 'date']
            ])
            ->add('countDaysArrive', 'integer', [
                'data' => 1
            ])
            ->add('countMen', 'choice', [
                'choices' => [1=>1,2=>2,3=>3,4=>4],
                'required' => true
            ])
            ->add('save', 'submit', ['label' => 'Search'])
            ->getForm();

        return $this->render(
            'UTSTestBundle:User:index.html.twig',
            ['form' => $form->createView()]
        );
    }

    public function makeQueryAction(Request $request)
    {
        if(!$request->isXmlHttpRequest()) {
            return new JsonResponse(['error'=>'Only ajax requests allowed here']);
        }

        $query = new Queries();

        $userName = $this->get('security.context')->getToken()->getUser()->getUserName();

        if(isset($_SERVER['REMOTE_ADDR'])) {
            $additionalInfo[] = $_SERVER['REMOTE_ADDR'];
        }

        if(isset($_SERVER['HTTP_USER_AGENT'])) {
            $additionalInfo[] = $_SERVER['HTTP_USER_AGENT'];
        }

        if(isset($additionalInfo)) {
            $userName .= ' (' . join(', ',$additionalInfo) . ')';
        }

        $query->setUserName($userName)
              ->setQuery($request->get('form[city]',null,true))
              ->setArriveDate(new \DateTime($request->get('form[arriveDate]',null,true)))
              ->setLeftDate(new \DateTime($request->get('form[leftDate]',null,true)))
              ->setCountDaysArrive($request->get('form[countDaysArrive]',null,true))
              ->setCountMen($request->get('form[countMen]',null,true));

        $em = $this->getDoctrine()->getManager();
        $em->persist($query);
        $em->flush();

        return new JsonResponse($query->getId());
    }
}
