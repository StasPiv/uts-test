<?php
/**
 * Created by PhpStorm.
 * User: Станислав
 * Date: 29.03.14
 * Time: 16:18
 */

namespace UTS\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UTS\TestBundle\Model\Search\Cities;

class ApiController extends Controller
{

    public function getCitiesAction(Request $request)
    {
        $foundCities = array_map(
            function($value)
            {
                if(isset($value['areaRu'])) {
                    $value['regionRu'] .= ', ' . $value['areaRu'];
                }

                if(isset($value['regionRu'])) {
                    $value['cityName'] .= ' (' . $value['regionRu'] . ')';
                }

                return $value['cityName'] . ' ' . $value['countryName'];
            },

            Cities::getCities($this->getDoctrine(), $request->get('term'))
        );

        return new JsonResponse($foundCities);
    }

} 