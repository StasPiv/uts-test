<?php
/**
 * Created by PhpStorm.
 * User: Станислав
 * Date: 28.03.14
 * Time: 16:44
 */

namespace UTS\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    public function indexAction(Request $request)
    {
        $request->setLocale('ru');

        $qb = $this->getDoctrine()->getManager()
                           ->getRepository('UTSTestBundle:Queries')
                           ->createQueryBuilder('q');

        $qb->select('q')
           ->orderBy('q.time','DESC');

        $query = $qb->getQuery();

        $paginator = $this->get('knp_paginator');

        $queries = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1)/*page number*/,
            20/*limit per page*/
        );

        return $this->render("UTSTestBundle:Admin:index.html.twig",
                            ['queries'=>$queries]);
    }

    public function deniedAction()
    {
        return $this->render("UTSTestBundle:Admin:denied.html.twig");
    }
} 