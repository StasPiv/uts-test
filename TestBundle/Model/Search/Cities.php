<?php
/**
 * Created by PhpStorm.
 * User: Станислав
 * Date: 29.03.14
 * Time: 23:39
 */

namespace UTS\TestBundle\Model\Search;

class Cities {

    public static function getCities($doctrine, $term)
    {
        $qb = $doctrine->getRepository('UTSTestBundle:Cities')
            ->createQueryBuilder('c');

        $qb->select('c.titleRu cityName, c.regionRu, c.areaRu, n.titleRu countryName')
            ->join('UTSTestBundle:Countries','n','WITH','n.countryId = c.countryId')
            ->where("c.titleRu LIKE :term")
            ->setParameter('term',$term.'%');

        $query = $qb->getQuery()->setMaxResults(100);

        return $query->getArrayResult();
    }

} 